"""backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework import routers
from api import views as v

router = routers.DefaultRouter(trailing_slash=False)
urlpatterns = router.urls
urlpatterns += [
    path('admin/', admin.site.urls),
    path('', v.index),
    path('api/login', v.login),
    path('api/register', v.register),
    path('api/index', v.index),
    path('api/index2', v.index2),
    path('api/prueba/all', v.all),
    path('api/prueba/save', v.save),
    path('api/prueba/edit/<str:id>', v.edit),
    path('api/prueba/update/<str:id>', v.update),
    path('api/prueba/delete/<str:id>', v.delete)
    # path('api/prueba/edit/{id}/$', v.edit)
]
