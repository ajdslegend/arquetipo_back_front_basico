from rest_framework_mongoengine import serializers

from .models import Prueba, User


class PruebaSerializer(serializers.DocumentSerializer):
    class Meta:
        model = Prueba
        fields = '__all__'


class UserSerializer(serializers.DocumentSerializer):
    class Meta:
        model = User
        fields = '__all__'
