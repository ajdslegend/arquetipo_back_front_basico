# from django.db import models
from mongoengine import Document, fields


# Create your models here.

class Prueba(Document):
    name = fields.StringField(required=True)
    age = fields.IntField(required=True)


class User(Document):
    username = fields.StringField(required=True)
    email = fields.StringField(required=True)
    password = fields.StringField(required=True)
