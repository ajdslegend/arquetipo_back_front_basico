import jwt
from rest_framework import status
from rest_framework.response import Response
from api.models import User
from backend.settings import SECRET_KEY

charset = 'utf-8'


def jwt_login_required(f):
    def wrapper(request, *args, **kwargs):
        try:
            data = request.META.get('HTTP_AUTHORIZATION').encode('utf-8', 'ignore').decode(charset)
            tokenAuth = str.replace(str(data), 'Bearer ', '')
            token = jwt.decode(tokenAuth, SECRET_KEY)
            ac = User.objects.get(email=token.get("email"))
            if ac:
                return f(request, *args, **kwargs)
        except Exception:
            data = {'success': False, 'message': "Error de token", 'data': None, 'exception': "error_exception"}
            return Response(data, status=status.HTTP_401_UNAUTHORIZED)

    wrapper.__doc__ = f.__doc__
    wrapper.__name__ = f.__name__
    return wrapper
