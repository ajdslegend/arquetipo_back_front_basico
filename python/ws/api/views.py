from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
import jwt
from django.contrib.auth.hashers import BCryptSHA256PasswordHasher as mkBcrypt
from .models import Prueba, User
from .serialize import PruebaSerializer, UserSerializer
from datetime import datetime, timedelta
from backend.settings import SECRET_KEY
from .decorators import jwt_login_required


# Create your views here.

@api_view(['GET'])
def index(request):
    serialize = [
        {
            "name": "ana"
        }, {
            "name": "luis"
        }
    ]
    data = {'success': True, 'message': "ok", 'data': serialize, 'exception': None}
    return Response(data, status=status.HTTP_200_OK)


@api_view(['POST'])
def index2(request):
    serialize = [
        {
            "id": 1,
            "name": "ana"
        },
        {
            "id": 2,
            "name": "luis"
        }
    ]
    data = {'success': True, 'message': "ok", 'data': serialize, 'exception': None}
    return Response(data, status=status.HTTP_200_OK)


@api_view(['POST'])
def register(request):
    try:
        data = request.data
        print("data: ", data)
        hasher = mkBcrypt()
        sl = hasher.salt()
        data["password"] = hasher.encode(data["password"], salt=sl)
        print("data1: ", data)
        serializer = UserSerializer(data=data)
        if serializer.is_valid():
            user = User(**data)
            user.save()
            serialize = serializer.data
            data = {'success': True, 'message': "ok", 'data': serialize, 'exception': None}
            return Response(data, status=status.HTTP_200_OK)
    except Exception:
        data = {'success': False, 'message': "Error al insertar los datos", 'data': None,
                'exception': "error_exception"}
        return Response(data, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def login(request):
    try:
        payload = request.data
        ac = User.objects.get(email=payload["email"])
        hasher = mkBcrypt()
        ps = hasher.verify(payload["password"], ac.password)
        if ps:
            usr = {
                'username': ac.username,
                'email': ac.email,
                'exp': datetime.utcnow() + timedelta(seconds=50000)
            }
            token = jwt.encode(usr, SECRET_KEY).decode('utf-8')
            tokens = {'token': (token), 'type': 'Bearer'}
            data = {'success': True, 'message': "ok", 'data': tokens, 'exception': None}
            return Response(data, status=status.HTTP_200_OK)
        else:
            data = {'success': False, 'message': "usuario y/o clave invalida", 'data': None,
                    'exception': 'error_exception'}
            return Response(data, status=status.HTTP_403_FORBIDDEN)
    except Exception:
        data = {'success': False, 'message': "Error usuario/clave no existe.Verifique bien los datos de entrada",
                'data': None, 'exception': "error_exception"}
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET'])
@jwt_login_required
def all(request):
    try:
        serializer = PruebaSerializer(Prueba.objects.all(), many=True)
        serialize = serializer.data
        data = {'success': True, 'message': "ok", 'data': serialize, 'exception': None}
        return Response(data, status=status.HTTP_200_OK)
    except Exception:
        data = {'success': False, 'message': "Verifique bien los datos de entrada",
                'data': None, 'exception': "error_exception"}
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['POST'])
@jwt_login_required
def save(request):
    try:
        data = request.data
        serializer = PruebaSerializer(data=data)
        if serializer.is_valid():
            prueba = Prueba(**data)
            prueba.save()
            serialize = serializer.data
            data = {'success': True, 'message': "ok", 'data': serialize, 'exception': None}
            return Response(data, status=status.HTTP_200_OK)
        else:
            data = {'success': False, 'message': "Verifique bien los datos de entrada",
                    'data': None, 'exception': "error_exception"}
            return Response(data, status=status.HTTP_401_UNAUTHORIZED)
    except Exception:
        data = {'success': False, 'message': "Verifique bien los datos de entrada",
                'data': None, 'exception': "error_exception"}
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET'])
@jwt_login_required
def edit(request, id):
    try:
        query_set = Prueba.objects.get(id=id)
        query_edit = query_set
        serializer = PruebaSerializer(query_edit)
        serialize = serializer.data
        data = {'success': True, 'message': "ok", 'data': serialize, 'exception': None}
        return Response(data, status=status.HTTP_200_OK)
    except Exception:
        data = {'success': False, 'message': "Verifique bien los datos de entrada",
                'data': None, 'exception': "error_exception"}
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['PUT'])
@jwt_login_required
def update(request, id):
    try:
        data = request.data
        query_set = Prueba.objects.get(id=id)
        serializer = PruebaSerializer(query_set, data=data)
        if serializer.is_valid():
            serializer.save()
            serialize = serializer.data
            data = {'success': True, 'message': "ok", 'data': serialize, 'exception': None}
            return Response(data, status=status.HTTP_200_OK)
        else:
            data = {'success': False, 'message': "Verifique bien los datos de entrada",
                    'data': None, 'exception': "error_exception"}
            return Response(data, status=status.HTTP_401_UNAUTHORIZED)
    except Exception:
        data = {'success': False, 'message': "Verifique bien los datos de entrada",
                'data': None, 'exception': "error_exception"}
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET'])
@jwt_login_required
def delete(request, id):
    try:
        query_set = Prueba.objects.get(id=id)
        query_set.delete()
        data = {'success': True, 'message': "ok", 'data': None, 'exception': None}
        return Response(data, status=status.HTTP_204_NO_CONTENT)
    except Exception:
        data = {'success': False, 'message': "Verifique bien los datos de entrada",
                'data': None, 'exception': "error_exception"}
        return Response(data, status=status.HTTP_401_UNAUTHORIZED)
