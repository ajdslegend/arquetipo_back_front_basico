package config

import (
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"github.com/jinzhu/gorm"
	"fmt"
	"io/ioutil"
	"encoding/json"
)

func StrDrive() string {
	env := EnviromentsRaw()
	psqlInfo := fmt.Sprintf(`host=%s port=%s user=%s password=%s dbname=%s sslmode=%s`,
		env.DbHost, env.DbPort, env.DbUser, env.DbPassword, env.DbName,env.DbSslmode)
	s := psqlInfo
	return s
}

func StrDialect() string {
	env := EnviromentsRaw()
	s := env.DbDialect
	return s
}

func Conn() {
	db, _ := gorm.Open("postgres", "host=localhost port=5432 user=admin dbname=db_gea_archetypt_node_master password=password sslmode=disable")
	defer db.Close()
}

func EnviromentsRaw() Enviroment {
	data, err := ioutil.ReadFile("config/config.json")
	if err != nil {
		fmt.Println(err)
	}

	// fmt.Println("#string(data): ",string(data))
	byteData := []byte(string(data))
	var enviroment Enviroment
	json.Unmarshal(byteData, &enviroment)

	return enviroment
}

type Enviroment struct {
	Domain       string `json:"domain"`
	RemoteHost   [] RemoteHost `json:"remote_host"`
	Port         string `json:"port"`
	DbDialect    string `json:"db_dialect"`
	DbHost       string `json:"db_host"`
	DbName       string `json:"db_name"`
	DbUser       string `json:"db_user"`
	DbPassword   string `json:"db_password"`
	DbSslmode    string `json:"db_sslmode"`
	DbPort       string `json:"db_port"`
	AppKey       string `json:"app_key"`
	SmtpHost     string `json:"smtp_host"`
	SmtpPort     string `json:"smtp_port"`
	SmtpEmail    string `json:"smtp_email"`
	SmtpPassword string `json:"smtp_password"`
	SmtpTls      string `json:"smtp_tls"`
}

type RemoteHost struct {
	Name string `json:"name"`
}
