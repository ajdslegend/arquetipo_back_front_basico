package util

import (
	"github.com/labstack/echo"
	"strings"
	"fmt"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"github.com/dgrijalva/jwt-go"
	"../model"
	"../config"
	"gopkg.in/go-playground/validator.v9"
	"golang.org/x/crypto/bcrypt"
	"log"
)

func SetToken(claims CustomClaims) string {
	//claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	tokensJWT := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	app_key:=config.EnviromentsRaw().AppKey
	key := []byte(app_key)

	tokenStr, er := tokensJWT.SignedString(key)
	if er != nil {
		fmt.Println("error: ", er)
	}

	return tokenStr
}

func DecodeHeaderToken(c echo.Context) ResponseU {
	var datas ResponseU
	r := c.Request().Header.Get("Authorization")
	if r == "" {
		datas = ResponseU{
			false,
			"Empty token",
			model.User{},
		}
	} else {
		headers := c.Request().Header
		token := headers.Get("Authorization")

		splitToken := strings.Split(token, " ")

		t := DecodeToken(splitToken[1])
		if t.Success == false {
			datas = ResponseU{
				false,
				"token invalid",
				model.User{},
			}
		} else {
			datas = ResponseU{
				true,
				"ok",
				t.Data,
			}
		}
	}

	return datas
}

func Auth(c echo.Context) User {
	user := c.Get("user").(string)
	fmt.Println("###: ", user)
	str := []byte(user)
	var raw User

	json.Unmarshal(str, &raw)

	return raw
}

func IsRead(c echo.Context, rol Rol) Response {
	var response Response
	var arrayPerm []string
	if rol.Rol != "" {
		arrayPerm = ArrayPermWithRol(c, rol)
	} else {
		arrayPerm = ArrayPerm(c, rol)
	}
	perm := CanPerm()
	index := IndexOf(perm[0], arrayPerm)

	if index != -1 {
		response = Response{
			true,
			"ok",
			"",
		}
	} else {
		response = Response{
			false,
			"error permission " + perm[0],
			"",
		}
	}

	return response
}

func IsCreate(c echo.Context, rol Rol) Response {

	var response Response
	var arrayPerm []string
	if rol.Rol != "" {
		arrayPerm = ArrayPermWithRol(c, rol)
	} else {
		arrayPerm = ArrayPerm(c, rol)
	}

	perm := CanPerm()
	index := IndexOf(perm[1], arrayPerm)

	if index != -1 {
		response = Response{
			true,
			"ok",
			"",
		}
	} else {
		response = Response{
			false,
			"error permission " + perm[1],
			"",
		}
	}

	return response
}

func IsEdit(c echo.Context, rol Rol) Response {

	var response Response
	var arrayPerm []string
	if rol.Rol != "" {
		arrayPerm = ArrayPermWithRol(c, rol)
	} else {
		arrayPerm = ArrayPerm(c, rol)
	}

	perm := CanPerm()
	index := IndexOf(perm[2], arrayPerm)
	if index != -1 {
		response = Response{
			true,
			"ok",
			"",
		}
	} else {
		response = Response{
			false,
			"error permission " + perm[2],
			"",
		}
	}

	return response
}

func IsUpdate(c echo.Context, rol Rol) Response {

	var response Response
	var arrayPerm []string
	if rol.Rol != "" {
		arrayPerm = ArrayPermWithRol(c, rol)
	} else {
		arrayPerm = ArrayPerm(c, rol)
	}

	perm := CanPerm()
	index := IndexOf(perm[3], arrayPerm)

	if index != -1 {
		response = Response{
			true,
			"ok",
			"",
		}
	} else {
		response = Response{
			false,
			"error permission " + perm[3],
			"",
		}
	}

	return response
}

func IsDelete(c echo.Context, rol Rol) Response {

	var response Response
	var arrayPerm []string
	if rol.Rol != "" {
		arrayPerm = ArrayPermWithRol(c, rol)
	} else {
		arrayPerm = ArrayPerm(c, rol)
	}

	perm := CanPerm()
	index := IndexOf(perm[4], arrayPerm)

	if index != -1 {
		response = Response{
			true,
			"ok",
			"",
		}
	} else {
		response = Response{
			false,
			"error permission " + perm[4],
			"",
		}
	}

	return response
}

func IndexOf(element string, data []string) (int) {
	for k, v := range data {
		if element == v {
			return k
		}
	}
	return -1
}

func CanPerm() []string {
	perm := []string{"read", "create", "edit", "update", "delete"}

	return perm
}

func ArrayPermWithRol(c echo.Context, rol Rol) []string {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	user := c.Get("user").(string)
	str := []byte(user)
	var raw User
	json.Unmarshal(str, &raw)
	var arrayPerm []string
	var profile model.Profile
	var permission model.Permission
	db.Debug().Where(map[string]interface{}{"user_id": raw.ID}).First(&profile)
	db.Debug().Where(map[string]interface{}{"id": profile.PermissionID}).First(&permission)
	decode := []byte((permission.ObjectModulePermission))
	var result Roles
	json.Unmarshal(decode, &result)
	if result.Rol == rol.Rol {
		for i := range result.Module {
			if result.Module[i].Acl == rol.Acl {
				arrayPerm = result.Module[i].Value
			}
		}
	}

	return arrayPerm
}

func ArrayPerm(c echo.Context, rol Rol) []string {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	user := c.Get("user").(string)
	str := []byte(user)
	var raw User
	json.Unmarshal(str, &raw)
	var arrayPerm []string
	var profile model.Profile
	var permission model.Permission
	db.Debug().Where(map[string]interface{}{"user_id": raw.ID}).First(&profile)
	db.Debug().Where(map[string]interface{}{"id": profile.PermissionID}).First(&permission)
	decode := []byte((permission.ObjectModulePermission))
	var result Roles
	json.Unmarshal(decode, &result)
	for i := range result.Module {
		if result.Module[i].Acl == rol.Acl {
			arrayPerm = result.Module[i].Value
		}
	}

	return arrayPerm
}

func ArrayPermMenu(c echo.Context) []Module {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	user := c.Get("user").(string)
	str := []byte(user)
	var raw User
	json.Unmarshal(str, &raw)
	var arrayPerms []Module
	var profile model.Profile
	var permission model.Permission
	db.Debug().Where(map[string]interface{}{"user_id": raw.ID}).First(&profile)
	db.Debug().Where(map[string]interface{}{"id": profile.PermissionID}).First(&permission)
	decode := []byte((permission.ObjectModulePermission))
	var result Roles
	json.Unmarshal(decode, &result)
	for i := range result.Module {
		if result.Module[i].Visible == true {
			arrayPerms = append(arrayPerms, Module{
				Url:          result.Module[i].Url,
				IconWeb:      result.Module[i].IconWeb,
				Mobile:       result.Module[i].Mobile,
				Android:      result.Module[i].Android,
				Ios:          result.Module[i].Ios,
				Ionic:        result.Module[i].Ionic,
				Name:         result.Module[i].Name,
				LangProperty: result.Module[i].LangProperty,
				Acl:          result.Module[i].Acl,
				Visible:      result.Module[i].Visible,
				Value:        result.Module[i].Value,
			})
		}
	}
	js, _ := json.Marshal(arrayPerms)
	json.Unmarshal([]byte(js), &arrayPerms)
	return arrayPerms
}

func DecodeToken(strToken string) ResponseU {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	var datas ResponseU

	if err != nil {
		fmt.Println(err)
	}

	if strings.HasPrefix(strToken, "") {
		datas = ResponseU{
			false,
			"token invalid",
			model.User{},
		}
	}
	app_key:=config.EnviromentsRaw().AppKey
	tokenObj, er := jwt.ParseWithClaims(strToken, &CustomClaims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(app_key), nil
	})

	var find model.User

	if claims, ok := tokenObj.Claims.(*CustomClaims); ok && tokenObj.Valid {
		//fmt.Printf("%vccc %v", claims.Email)
		db.Debug().Where(map[string]interface{}{"email": claims.Email}).Find(&find)
		// fmt.Printf("%v %v", claims.Email)
		datas = ResponseU{
			true,
			"ok",
			find,
		}
	} else {
		fmt.Printf("#####er", er)
		datas = ResponseU{
			false,
			"token invalid",
			model.User{},
		}
	}

	return datas
}

func HashAndSalt(pwd []byte) string {

	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.MinCost)
	if err != nil {
		log.Println(err)
	}
	// GenerateFromPassword returns a byte slice so we need to
	// convert the bytes to a string and return it
	return string(hash)
}

func ComparePasswords(hashedPwd string, plainPwd []byte) bool {
	// Since we'll be getting the hashed password from the DB it
	// will be a string so we'll need to convert it to a byte slice
	byteHash := []byte(hashedPwd)
	err := bcrypt.CompareHashAndPassword(byteHash, plainPwd)
	if err != nil {
		log.Println(err)
		return false
	}

	return true
}

func (cv *CustomValidator) Validate(i interface{}) error {
	return cv.Validator.Struct(i)
}

type Roles struct {
	Rol    string   `json:"rol"`
	Module []Module `json:"module"`
}

type Rol struct {
	Rol    string `json:"rol"`
	Module Module `json:"module"`
	Acl    string `json:"acl"`
}

type Module struct {
	Url          string    `json:"url"`
	IconWeb      string    `json:"iconWeb"`
	Mobile       string    `json:"mobile"`
	Android      string    `json:"android"`
	Ios          string    `json:"ios"`
	Ionic        string    `json:"ionic"`
	Name         string    `json:"name"`
	LangProperty string    `json:"lang_property"`
	Acl          string    `json:"acl"`
	Visible      bool      `json:"visible"`
	Value        [] string `json:"value"`
}

type User struct {
	ID       uint   `json:"id"`
	Avatar   string `json:"avatar"`
	Username string `json:"username"`
	Email    string `json:"email"`
}

type Response struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type ResponseU struct {
	Success bool       `json:"success"`
	Message string     `json:"message"`
	Data    model.User `json:"data"`
}

type Token struct {
	Token string `json:"token"`
}

type TokenDecode struct {
	ID    string
	Email string
	jwt.StandardClaims
}

type CustomClaims struct {
	ID    uint
	Email string
	jwt.StandardClaims
}

type Paginate struct {
	Page  int `json:"page"`
	Limit int `json:"limit"`
}

type CustomValidator struct {
	Validator *validator.Validate
}
