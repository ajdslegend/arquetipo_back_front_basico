package test

import (
	"github.com/labstack/echo"
	"github.com/jinzhu/gorm"
	"fmt"
	"../../config"
	"../../model"
	"../../util"
	"net/http"
	"strconv"
)

func SelectTest(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	var rol util.Rol
	rol.Acl = "test"
	var datas util.Response

	if util.IsRead(c, rol).Success == true {

		var t [] model.Test
		// Migrate the schema
		db.Debug().Find(&t)
		datas = util.Response{
			true,
			"ok",
			t,
		}
	} else {
		datas = util.IsRead(c, rol)
	}

	return c.JSON(http.StatusAccepted, datas)
}

func CreateTest(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	var rol util.Rol
	rol.Acl = "test"
	var datas util.Response

	if util.IsCreate(c, rol).Success == true {

		t := new(model.Test)
		errors := c.Bind(&t)
		if errors != nil {
			return nil
		}
		//fmt.Println("b: ", m)
		rt := db.Debug().Create(&t)
		datas = util.Response{
			true,
			"ok",
			rt.Value,
		}
	} else {
		datas = util.IsCreate(c, rol)
	}
	return c.JSON(http.StatusAccepted, datas)
}

func EditTest(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	var rol util.Rol
	rol.Acl = "test"
	var datas util.Response

	if util.IsEdit(c, rol).Success == true {
		s := c.Param("id")
		//map[string]interface{}{"name": "jinzhu", "age": 20}
		id, _ := strconv.Atoi(s)

		rt := db.Debug().Where(map[string]interface{}{"id": id}).Find(&model.Test{})

		datas = util.Response{
			true,
			"ok",
			rt.Value,
		}
	} else {
		datas = util.IsEdit(c, rol)
	}
	return c.JSON(http.StatusAccepted, datas)
}

func UpdateTest(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	var rol util.Rol
	rol.Acl = "test"
	var datas util.Response

	if util.IsUpdate(c, rol).Success == true {
		s := c.Param("id")
		//map[string]interface{}{"name": "jinzhu", "age": 20}
		id, _ := strconv.Atoi(s)

		t := new(model.Test)
		errors := c.Bind(&t)
		if errors != nil {
			return nil
		}

		rt := db.Debug().Where(map[string]interface{}{"id": id}).Find(&model.Test{})
		rt.Update(&t)

		datas = util.Response{
			true,
			"ok",
			rt.Value,
		}
	} else {
		datas = util.IsUpdate(c, rol)
	}
	return c.JSON(http.StatusAccepted, datas)
}

func DeleteTest(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	var rol util.Rol
	rol.Acl = "test"
	var datas util.Response

	if util.IsDelete(c, rol).Success == true {

		s := c.Param("id")
		id, _ := strconv.Atoi(s)

		rt := db.Debug().Where(map[string]interface{}{"id": id}).Find(&model.Test{})
		rt.Delete(&model.Test{})

		datas = util.Response{
			true,
			"ok",
			rt.Value,
		}
	} else {
		datas = util.IsDelete(c, rol)
	}
	return c.JSON(http.StatusAccepted, datas)
}
