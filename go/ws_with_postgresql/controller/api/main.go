package api

import (
	//import b64 "encoding/base64"
	"github.com/labstack/echo"
	"github.com/jinzhu/gorm"
	"github.com/biezhi/gorm-paginator/pagination"
	"../../config"
	"../../model"
	"../../util"
	b64 "encoding/base64"
	"fmt"
	"net/http"
	"strconv"
	"encoding/json"
	"bytes"
	"io/ioutil"
	"strings"
)

func CreateApi(c echo.Context) error {
	raw := util.Auth(c)
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	t := new(model.Api)
	errors := c.Bind(&t)
	if errors != nil {
		return nil
	}
	// map[string]interface{}{"id": id}
	// import b64 "encoding/base64"
	objtoken := map[string]interface{}{
		"domain":  t.Domain,
		"user_id": raw.ID,
		"title":   t.Content,
		"content": t.Content, "active": t.Active,
	}
	strToken, _ := json.Marshal(objtoken)
	strEncryptToken := b64.StdEncoding.EncodeToString([]byte(strToken))
	strDecryptToken, _ := b64.StdEncoding.DecodeString(strEncryptToken)
	fmt.Println("#token decrypt: ", string(strDecryptToken))
	tt := model.Api{
		Token:   strEncryptToken,
		Title:   t.Title,
		Content: t.Content,
		Domain:  t.Domain,
		Active:  t.Active,
		UserID:  raw.ID,
	}
	//rt := db.Debug().Create(&tt)
	db.Debug().Create(&tt)
	datas := util.Response{
		true,
		"ok",
		tt,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func SelectApi(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	var t [] model.Api
	//rt := db.Debug().Related(&u).Find(&t)
	//field:=[]string{"token", "domain"}
	//rt := db.Debug().Preload("User").Select(field).Find(&t)
	//rt := db.Debug().Preload("User").Find(&t)
	// db.Where("name LIKE ?", "%jin%").Find(&users)
	db.Debug().Preload("User").Find(&t)
	datas := util.Response{
		true,
		"ok",
		t,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func PageApi(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	s0 := c.Param("offset")
	//map[string]interface{}{"name": "jinzhu", "age": 20} //[]string{"name", "age"}
	offset, _ := strconv.Atoi(s0)

	s1 := c.Param("limits")
	//map[string]interface{}{"name": "jinzhu", "age": 20} //[]string{"name", "age"}
	limits, _ := strconv.Atoi(s1)

	var t [] model.Api
	//rt := db.Debug().Related(&u).Find(&t)
	//field:=[]string{"token", "domain"}
	//rt := db.Debug().Preload("User").Select(field).Find(&t)
	orderBy := []string{"id desc"}
	rt := db.Debug().Preload("User").Find(&t)
	//db.Debug().Preload("User").Find(&t)

	//p := pagination.Paging(&pagination.Param{
	//	DB:      rt,
	//	Page:    offset,
	//	Limit:   limits,
	//	OrderBy: orderBy,
	//}, &t)

	p := pagination.Pagging(&pagination.Param{
		DB:      rt,
		Page:    offset,
		Limit:   limits,
		OrderBy: orderBy,
	}, &t)

	datas := util.Response{
		true,
		"ok",
		p,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func PagesApi(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	payload := new(util.Paginate)
	errors := c.Bind(&payload)
	if errors != nil {
		return nil
	}

	var t [] model.Api
	orderBy := []string{"id desc"}
	rt := db.Debug().Preload("User").Find(&t)

	//p := pagination.Paging(&pagination.Param{
	//	DB:      rt,
	//	Page:    payload.Page,
	//	Limit:   payload.Limit,
	//	OrderBy: orderBy,
	//}, &t)

	p := pagination.Pagging(&pagination.Param{
		DB:      rt,
		Page:    payload.Page,
		Limit:   payload.Limit,
		OrderBy: orderBy,
	}, &t)

	datas := util.Response{
		true,
		"ok",
		p,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func SearchApi(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	payload := new(model.Api)
	errors := c.Bind(&payload)
	if errors != nil {
		return nil
	}

	var t [] model.Api
	db.Debug().Preload("User").Where("domain LIKE ?", "%"+strings.ToLower(payload.Domain)+"%").Find(&t)
	datas := util.Response{
		true,
		"ok",
		t,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func SearchByTokenApi(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	payload := new(model.Api)
	errors := c.Bind(&payload)
	if errors != nil {
		return nil
	}

	var t  model.Api
	db.Debug().Preload("User").Where("token = ?", payload.Token).Find(&t)
	datas := util.Response{
		true,
		"ok",
		t,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func EditApi(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	s := c.Param("id")
	//map[string]interface{}{"name": "jinzhu", "age": 20} //[]string{"name", "age"}
	id, _ := strconv.Atoi(s)
	var api model.Api
	//field:=[]string{"token", "domain"}
	//rt := db.Debug().Preload("User").Select(field).Where(map[string]interface{}{"id": id}).Find(&model.Api{})
	//rt := db.Debug().Preload("User").Where(map[string]interface{}{"id": id}).Find(&model.Api{})
	db.Debug().Preload("User").Where(map[string]interface{}{"id": id}).Find(&api)

	datas := util.Response{
		true,
		"ok",
		api,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func UpdateApi(c echo.Context) error {
	// raw := util.Auth(c)
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	s := c.Param("id")
	//map[string]interface{}{"name": "jinzhu", "age": 20}
	id, _ := strconv.Atoi(s)

	t := new(model.Api)
	errors := c.Bind(&t)
	if errors != nil {
		return nil
	}

	objtoken := map[string]interface{}{"domain": t.Domain, "user_id": t.UserID, "title": t.Content, "content": t.Content, "active": t.Active}
	strToken, _ := json.Marshal(objtoken)
	strEncryptToken := b64.StdEncoding.EncodeToString([]byte(strToken))
	strDecryptToken, _ := b64.StdEncoding.DecodeString(strEncryptToken)
	fmt.Println("#token decrypt: ", string(strDecryptToken))

	tt := model.Api{
		Token:   strEncryptToken,
		Title:   t.Title,
		Content: t.Content,
		Domain:  t.Domain,
		Active:  t.Active,
		UserID:  t.UserID,
	}

	rt := db.Debug().Where(map[string]interface{}{"id": id}).Find(&model.Api{})
	rt.Update(&tt)

	datas := util.Response{
		true,
		"ok",
		rt.Value,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func DeleteApi(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	s := c.Param("id")
	id, _ := strconv.Atoi(s)

	rt := db.Debug().Where(map[string]interface{}{"id": id}).Find(&model.Api{})
	rt.Delete(&model.Api{})

	datas := util.Response{
		true,
		"ok",
		rt.Value,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func CreateApiInstance(c echo.Context) error {

	t := new(model.Api)
	errors := c.Bind(&t)
	if errors != nil {
		return nil
	}

	objtoken := map[string]interface{}{
		"credential_token": t.Token,
	}
	strToken, _ := json.Marshal(objtoken)
	jsonstr := []byte(strToken)
	remote_host0 := config.EnviromentsRaw().RemoteHost[0].Name
	//url := "http://localhost:1324/api/instance/create"
	url := remote_host0 + "/api/instance/create"
	contentType := "application/json"
	req, _ := http.NewRequest("POST", url, bytes.NewBuffer(jsonstr))
	req.Header.Set("Content-Type", contentType)
	client := http.Client{}
	resp, _ := client.Do(req)

	defer resp.Body.Close()
	fmt.Println("##response remote: ", resp)

	body, _ := ioutil.ReadAll(resp.Body)

	strBody := string(body)

	strBodyByte := []byte(strBody)
	var raw model.ResponseApiInstance
	json.Unmarshal(strBodyByte, &raw)

	datas := util.Response{
		Success: true,
		Message: "ok",
		Data:    raw.Data,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func SelectApiInstace(c echo.Context) error {
	//url := "http://localhost:1324/api/instance/all"
	remote_host0 := config.EnviromentsRaw().RemoteHost[0].Name

	url := remote_host0 + "/api/instance/all"
	contentType := "application/json"
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("Content-Type", contentType)
	client := http.Client{}
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	fmt.Println("##response remote: ", resp)
	body, _ := ioutil.ReadAll(resp.Body)

	strBody := string(body)
	strBodyByte := []byte(strBody)
	var raw model.ResponseArrayApiInstance
	json.Unmarshal(strBodyByte, &raw)

	datas := util.Response{
		Success: true,
		Message: "ok",
		Data:    raw.Data,
	}
	return c.JSON(http.StatusAccepted, datas)
}
