package controller

import (
	"github.com/labstack/echo"
	"net/http"
	"../util"
)

func Hellos(c echo.Context) error {

	json := util.Response{
		true,
		"Hello, World!",
		nil,
	}
	return c.JSON(http.StatusOK, json)
}

func Restricted(c echo.Context) error {
	raw := util.Auth(c)
	name := raw.Email
	return c.String(http.StatusOK, "Welcome "+name+"!")
}

func HelloW(c echo.Context) error {
	return c.String(http.StatusOK, "Hello, World!")
}
