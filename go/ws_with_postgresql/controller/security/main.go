package security

import (
	"github.com/labstack/echo"
	"github.com/jinzhu/gorm"
	"fmt"
	"net/http"
	"../../config"
	"../../model"
	"../../util"
	"github.com/dgrijalva/jwt-go"
	"time"
	"io/ioutil"
	"encoding/json"
)

func Register(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	t := new(model.User)
	errors := c.Bind(&t)
	if errors != nil {
		return nil
	}

	//if errs := c.Validate(t); errs != nil {
	//	fmt.Println(errs)
	//	return nil
	//}

	byteP := []byte(t.Password)
	p := util.HashAndSalt(byteP)
	tt := model.User{
		Avatar:   t.Avatar,
		Username: t.Username,
		Email:    t.Email,
		Password: p,
	}

	//fmt.Println("b: ", m)
	rt := db.Debug().Create(&tt)
	datas := util.Response{
		true,
		"ok",
		rt.Value,
	}
	return c.JSON(http.StatusAccepted, datas)
}

func Login(c echo.Context) error {
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	var datas util.Response

	t := new(model.User)
	errors := c.Bind(&t)
	if errors != nil {
		return nil
	}

	if errs := c.Validate(t); errs == nil {
		fmt.Println("#errs validate: ",errs.Error())
		datas = util.Response{
			false,
			"error_validate",
			nil,
		}
	}

	var find model.User

	db.Debug().Where(map[string]interface{}{"email": t.Email}).Find(&find)

	// fmt.Println("dins: ",find.Email)

	byteP := []byte(t.Password)

	if util.ComparePasswords(find.Password, byteP) {
		var id = find.ID
		claims := util.CustomClaims{
			ID:    id,
			Email: find.Email,
			StandardClaims: jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			},
		}
		tokenStr := util.SetToken(claims)

		strTo := util.Token{Token: tokenStr}

		datas = util.Response{
			true,
			"ok",
			strTo,
		}
	} else {
		datas = util.Response{
			false,
			"error_email_password_mismatch",
			nil,
		}
	}

	return c.JSON(http.StatusAccepted, datas)
}

func UserInfo(c echo.Context) error {
	var datas util.Response
	raw := util.Auth(c)
	if raw.Email == "" {
		datas = util.Response{
			false,
			"error",
			nil,
		}
	}

	datas = util.Response{
		true,
		"ok",
		raw,
	}

	return c.JSON(http.StatusAccepted, datas)
}

func Menu(c echo.Context) error {
	var datas util.Response

	menu := util.ArrayPermMenu(c)
	fmt.Println("#menu: ", menu)
	if len(menu) != 0 {
		datas = util.Response{
			true,
			"ok",
			menu,
		}
	} else {
		datas = util.Response{
			false,
			"menu empty",
			"",
		}
	}

	return c.JSON(http.StatusAccepted, datas)
}

func PermissionFaker(c echo.Context) error {
	var datas util.Response
	db, err := gorm.Open(config.StrDialect(), config.StrDrive())
	defer db.Close()

	if err != nil {
		fmt.Println(err)
	}

	data, err := ioutil.ReadFile("FileSystem/permissionFaker.json")
	if err != nil {
		fmt.Println(err)
	}

	//fmt.Println("#strData: ", string(data))

	data1, err1 := ioutil.ReadFile("FileSystem/permissionFakerOld.json")
	if err1 != nil {
		fmt.Println(err1)
	}

	//fmt.Println("#strData: ", string(data1))

	var perm []model.Permission
	count := "*"
	db.Debug().Find(&perm).Count(&count)

	decode := []byte(string(data))
	var result []interface{}
	json.Unmarshal(decode, &result)

	decode1 := []byte(string(data1))
	var result1 []interface{}
	json.Unmarshal(decode1, &result1)

	if len(perm) == 0 {
		for i := range result {
			fmt.Println("##i: ", result[i])
			js, _ := json.Marshal(result[i])
			fmt.Println("##js: ", string(js))

			t := model.Permission{
				ObjectModulePermission: string(js),
			}

			db.Debug().Create(&t)
		}
	} else {
		for i := range result1 {
			js1, _ := json.Marshal(result1[i])
			var perms model.Permission
			db.Debug().Where(map[string]interface{}{"object_module_permission": string(js1)}).First(&perms)
			permissionx := result
			for j := range permissionx {
				fmt.Println(j)
				fmt.Println("##perms.id: ", perms.ID)
				perms.ObjectModulePermission = string(js1)
			}

			db.Save(&perms)
			arrayData, _ := json.Marshal(permissionx)
			byteArrayData := []byte(arrayData)

			err := ioutil.WriteFile("FileSystem/permissionFakerOld.json", byteArrayData, 0777)
			if err != nil {
				fmt.Println("err file: ", err)
			}
		}

	}

	datas = util.Response{
		true,
		"ok",
		"",
	}

	return c.JSON(http.StatusAccepted, datas)
}

func PermissionGet(c echo.Context) error {
	var datas util.Response

	data, err := ioutil.ReadFile("FileSystem/permissionFaker.json")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("#strData: ", string(data))
	decode := []byte(string(data))
	var result []interface{}
	json.Unmarshal(decode, &result)

	datas = util.Response{
		true,
		"ok",
		result,
	}

	return c.JSON(http.StatusAccepted, datas)
}

func PermissionPut(c echo.Context) error {
	var datas util.Response

	arrayData := `[{
		"rol": "users",
		"module": [
		{
			"url": "",
			"iconWeb": "",
			"mobile": "",
			"android": "",
			"ios": "",
			"ionic": "",
			"name": "",
			"lang_property": "",
			"acl": "",
			"visible": true,
			"value": ["read", "create", "edit", "update", "delete"]
		}
	]
	}]`

	fmt.Println("arrayData: ", arrayData)

	byteArrayData := []byte(arrayData)

	err := ioutil.WriteFile("FileSystem/permissionFaker.json", byteArrayData, 0777)
	if err != nil {
		fmt.Println("err file: ", err)
	}

	datas = util.Response{
		true,
		"ok",
		"",
	}

	return c.JSON(http.StatusAccepted, datas)
}
