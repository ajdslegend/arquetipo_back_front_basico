package middlewares

import (
	"github.com/labstack/echo"
	"../model"
	"net/http"
	"fmt"
	"reflect"
	"../util"
	json2 "encoding/json"
)

type Resp struct {
	Success bool
	Message string
	User    model.User
}

func AuthJWT(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		var datas util.Response
		t := util.DecodeHeaderToken(c)
		if t.Success == true {
			v := reflect.ValueOf(t.Data)
			values := make(map[string]interface{}, v.NumField())
			m :=  (t.Data)
			fmt.Println("<- ", v.Interface())
			fmt.Println("-> ", m)
			fmt.Println("<-> ", values)

			json, _ :=json2.Marshal(&t.Data)
			fmt.Println("<josn> ", string(json))
			//c.Set("user", values)
			//c.Set("user", t.Data)
			c.Set("user", string(json))

			return next(c)
		} else {
			datas.Success = t.Success
			datas.Message = t.Message
			datas.Data = ""
		}

		return c.JSON(http.StatusUnauthorized, datas)
	}
}
