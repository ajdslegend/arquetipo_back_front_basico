package model

import (
	"time"
)

type Test struct {
	//gorm.Model
	ID  uint `gorm:"primary_key" json:"id"` // `json:"success"`
	Name string `json:"name"`
	Description string `json:"description"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	// DeletedAt *time.Time
	//DeletedAt time.Time `gorm:"-"`   // Ignore this field
}

type Api struct {
	//gorm.Model
	ID  uint `gorm:"primary_key" json:"id"`
	Token string `json:"token"`
	Domain string `json:"domain"`
	Title string `json:"title"`
	Content string `json:"content"`
	Active bool `json:"active"`
	UserID uint `json:"user_id"`
	User User `gorm:"foreignkey:UserID" json:"user"` // `gorm:"foreignkey:UserRefer"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
	// DeletedAt *time.Time
	//DeletedAt time.Time `gorm:"-"`   // Ignore this field
}

type User struct {
	ID  uint `gorm:"primary_key" json:"id"`
	Avatar string `json:"avatar"`
	Username string `json:"username" validate:"required"`
	Email string `json:"email" validate:"required"`
	Password string `json:"password" validate:"required"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

//type User struct {
//	ID  uint `gorm:"primary_key" json:"id"`
//	Avatar string `json:"avatar"`
//	Username string `json:"username"`
//	Email string `json:"email"`
//	Password string `json:"password"`
//	CreatedAt time.Time `json:"created_at"`
//	UpdatedAt time.Time `json:"updated_at"`
//}

type Permission struct {
	ID  uint `gorm:"primary_key" json:"id"`
	ObjectModulePermission string `json:"object_module_permission"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type Profile struct {
	ID  uint `gorm:"primary_key" json:"id"`
	UserID  uint `json:"user_id"`
	PermissionID  uint `json:"permission_id"`
	User User `gorm:"foreignkey:UserID" json:"user"`
	Permission Permission `gorm:"foreignkey:PermissionID" json:"permission"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}


type ApiInstance struct {
	//ID  uint `gorm:"primary_key" json:"id"`
	CredentialToken string `json:"credential_token"`
	CredentialB64 string `json:"credential_b64"`
	CredentialBcrypt string `json:"credential_bcrypt"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

type ResponseApiInstance struct {
	Success bool  `json:"success"`
	Message string `json:"message"`
	Data ApiInstance `json:"data"`
}

type ResponseArrayApiInstance struct {
	Success bool  `json:"success"`
	Message string `json:"message"`
	Data []ApiInstance `json:"data"`
}

//func (Test) TableName() string {
//	return "tests"
//}