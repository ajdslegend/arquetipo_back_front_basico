package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"./controller"
	"./controller/security"
	"./controller/api"
	"./controller/test"
	"./middlewares"
	"./util"
	"gopkg.in/go-playground/validator.v9"
)

func main() {

	app := echo.New()

	app.Validator = &util.CustomValidator{Validator: validator.New()}

	app.Static("/static", "assets")

	app.Use(middleware.BodyLimit("200M"))

	app.Use(middleware.Logger())
	app.Use(middleware.Recover())

	//CORS
	app.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.HEAD, echo.PUT, echo.PATCH, echo.POST, echo.DELETE},
	}))

	r := app.Group("")
	r.Use(middlewares.AuthJWT)

	//fmt.Println(config)
	// Routes
	//app.Group("/api/v1")
	app.GET("/", controller.Hellos)

	// Route Security
	app.POST("/register", security.Register)

	app.POST("/login", security.Login)

	app.GET("/permission-put", security.PermissionPut)
	app.GET("/permission-get", security.PermissionGet)
	app.GET("/permission-faker", security.PermissionFaker)

	r.GET("/auth", controller.Hellos)
	r.GET("/auth2", controller.Restricted)
	r.GET("/api/user-info", security.UserInfo)
	r.GET("/api/menu", security.Menu)
	//
	r.POST("/api/create", api.CreateApi)
	r.GET("/api/all", api.SelectApi)
	r.GET("/api/page/:offset/:limits", api.PageApi)
	r.POST("/api/pages", api.PagesApi)
	app.POST("/api-search-by-token", api.SearchByTokenApi)
	r.POST("/api/read-paginate", api.PagesApi)
	r.POST("/api/search", api.SearchApi)
	r.GET("/api/edit/:id", api.EditApi)
	r.PUT("/api/update/:id", api.UpdateApi)
	r.GET("/api/delete/:id", api.DeleteApi)

	r.GET("/test/all", test.SelectTest)
	r.POST("/test/create", test.CreateTest)
	r.GET("/test/edit/:id", test.EditTest)
	r.PUT("/test/update/:id", test.UpdateTest)
	r.GET("/test/delete/:id", test.DeleteTest)

	r.POST("/api/instances/dbtest/create", api.CreateApiInstance)
	r.GET("/api/instances/dbtest/all", api.SelectApiInstace)

	// Start server
	app.Logger.Fatal(app.Start(":1323"))
}
