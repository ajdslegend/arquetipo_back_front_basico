import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import {RouterModule, Routes} from '@angular/router';
// import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
// import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { LoginComponent } from './components/login/login.component';
import { IndexComponent } from './components/index/index.component';

import { HttpInterceptorToken } from './interceptor/http-interceptor-token';

import { SecurityService } from './services/security.service';
import { PruebaService } from './services/prueba.service';
import { HttpProviders } from './common/http-providers';
import { CommonService } from './common/common.service';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    IndexComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    HttpClientModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    ReactiveFormsModule,
    FormsModule,
    RouterModule,
  ],
  providers: [
    HttpProviders,
    CommonService,
    SecurityService,
    PruebaService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptorToken,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
