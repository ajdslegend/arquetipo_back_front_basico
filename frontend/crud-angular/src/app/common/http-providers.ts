import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable()
export class HttpProviders {
    constructor(public http: HttpClient) {
    }

    get(endpoint: string, reqOpts?: any): Observable<any> {
        return this.http.get(endpoint, reqOpts).pipe(map(this.extractData), catchError(this.handleError));
    }

    post(endpoint: string, body: any, reqOpts?: any): Observable<any> {
        return this.http.post(endpoint, body, reqOpts).pipe(map(this.extractData), catchError(this.handleError));
    }

    put(endpoint: string, body: any, reqOpts?: any): Observable<any> {
        return this.http.put(endpoint, body, reqOpts).pipe(map(this.extractData), catchError(this.handleError));
    }

    delete(endpoint: string, reqOpts?: any): Observable<any> {
        return this.http.delete(endpoint, reqOpts).pipe(map(this.extractData), catchError(this.handleError));
    }

    patch(endpoint: string, body: any, reqOpts?: any): Observable<any> {
        return this.http.patch(endpoint, body, reqOpts).pipe(map(this.extractData), catchError(this.handleError));
    }


    protected extractData(parameters): Observable<any> {
        // console.log("success: "+JSON.stringify(parameters));
        let res: any;
        res = (parameters);
        let body: any;
        body = res;
        // console.log('body ' + body);
        return body || {};
    }

    protected handleError(parameters): Observable<any> {
        try {
            let error;
            error = parameters;
            // console.log("error: "+JSON.stringify(error));
            if (error.status === 401) {
                // A client-side or network error occurred. Handle it accordingly.
                console.log('An error occurred:', error.message);
            }
            const er = error.message;
            return throwError(er);
        } catch (e) {
            return throwError(e);
        }
    }
}
