import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor() { }

  public getResultData(result: Object): any {
    let obj: any;
    if (result.hasOwnProperty('success')) {
      if (result['success'] === true) {
        if (result.hasOwnProperty('data')) {
          obj = result['data'];
        }
      }
    }
    return obj;
  }

}
