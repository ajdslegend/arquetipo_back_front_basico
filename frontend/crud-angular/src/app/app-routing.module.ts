import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Constant } from './constant/constant';
import { LoginComponent } from './components/login/login.component';
import { LoginEnableGuard } from './guard/login-enable.guard';
import { IndexComponent } from './components/index/index.component';
import { LoginDisableGuard } from './guard/login-disable.guard';

const routes: Routes = [
  { path: '', redirectTo: Constant.ROUTE.REDIRECT_TO, pathMatch: 'full' },
  { path: Constant.ROUTE.LOGIN, component: LoginComponent, canActivate: [LoginDisableGuard] },
  { path: Constant.ROUTE.INDEX, component: IndexComponent, canActivate: [LoginEnableGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    useHash: true
    , enableTracing: false
    , anchorScrolling: 'enabled'
    , onSameUrlNavigation: 'reload'
    , scrollPositionRestoration: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
