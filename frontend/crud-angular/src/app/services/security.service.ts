import { Injectable } from '@angular/core';
import { HttpProviders } from '../common/http-providers';
import { Constant } from '../constant/constant';

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(private _http: HttpProviders
  ) {
  }

  public login(payload: any): Promise<any> {
    let endpoint: any;
    endpoint = Constant.ENDPOINT.LOGIN_HASH;
    return this._http.post(endpoint, payload).toPromise();
  }
}
