import { Injectable } from '@angular/core';
import { HttpProviders } from '../common/http-providers';
import { Constant } from '../constant/constant';

@Injectable({
  providedIn: 'root'
})
export class PruebaService {

  constructor(private _http: HttpProviders
  ) {
  }

  public all(): Promise<any> {
    let endpoint: any;
    endpoint = Constant.ENDPOINT.PRUEBA_ALL;
    return this._http.get(endpoint).toPromise();
  }

  public save(payload: any): Promise<any> {
    let endpoint: any;
    endpoint = Constant.ENDPOINT.PRUEBA_SAVE;
    return this._http.post(endpoint, payload).toPromise();
  }

  public edit(id: any): Promise<any> {
    let endpoint: any;
    endpoint = Constant.ENDPOINT.PRUEBA_EDIT + id;
    return this._http.get(endpoint).toPromise();
  }

  public update(payload: any, id: any): Promise<any> {
    let endpoint: any;
    endpoint = Constant.ENDPOINT.PRUEBA_UPDATE + id;
    return this._http.put(endpoint, payload).toPromise();
  }

  public delete(id: any): Promise<any> {
    let endpoint: any;
    endpoint = Constant.ENDPOINT.PRUEBA_DELETE + id;
    return this._http.get(endpoint).toPromise();
  }
}
