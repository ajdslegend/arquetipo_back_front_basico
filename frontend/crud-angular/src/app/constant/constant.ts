import { environment } from '../../environments/environment';
export class Constant {
    private static API = environment.apiURL + '/api/v1/';
    public static ENDPOINT = {
        LOGIN: Constant.API + 'login',
        LOGIN_HASH: Constant.API + 'login-hash',
        PRUEBA_ALL: Constant.API + 'prueba/all',
        PRUEBA_SAVE: Constant.API + 'prueba/save',
        PRUEBA_EDIT: Constant.API + 'prueba/edit/',
        PRUEBA_UPDATE: Constant.API + 'prueba/update/',
        PRUEBA_DELETE: Constant.API + 'prueba/delete/',
    }

    public static ROUTE = {
        REDIRECT_TO: 'index',
        LOGIN: 'login',
        INDEX: 'index',
    }
}
