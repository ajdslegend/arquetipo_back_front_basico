import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { CommonService } from '../../common/common.service';
import { Router } from '@angular/router';
import { PruebaService } from '../../services/prueba.service';

import { Constant } from '../../constant/constant';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  model: FormGroup;
  data: any;
  fields: any;
  ID: any;

  constructor(private _common: CommonService,
    private _route: Router,
    private _prueba: PruebaService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.initForm();
    this.initComponent();
    this.getAll();

  }

  logout() {
    console.log("logout");
    setTimeout(() => {
      localStorage.removeItem('token');
      this._route.navigate(['./' + Constant.ROUTE.LOGIN]);
    }, 100);
  }

  initForm() {
    this.model = this.formBuilder.group({
      name: [null, [Validators.required]],
      age: [null, [Validators.required, Validators.pattern("^[0-9]*$")]]
    });
  }

  submit(form: NgForm) {
    console.log(form);
    let payload = JSON.stringify(form);

    this._prueba.save(payload).then((res) => {
      if (res['success'] === true) {
        this.initComponent();
        this.getAll();
        this.model.reset();
      }
    })

  }

  getAll() {
    this._prueba.all().then((res) => {
      if (res['success'] === true) {
        this.data = this._common.getResultData(res);
        let keys: any = [];
        let jsonData: any = this.data;
        for (let i = 0; i < jsonData.length; i++) {
          Object.keys(jsonData[i]).forEach(function (key) {
            if (keys.indexOf(key) == -1) {
              if (key !== "id") {
                keys.push(key);
              }
            }
          });
        }

        this.fields = keys;
      }
    })
  }

  edit(id: any) {
    this._prueba.edit(id).then((res) => {
      if (res["success"] === true) {
        let data = this._common.getResultData(res);
        this.model.get('name').setValue(data["name"]);
        this.model.get('age').setValue(data["age"]);
        this.ID = id;
        this.editComponent();

      }
    });
  }

  update(form: NgForm) {
    let payload = JSON.stringify(form);
    this._prueba.update(payload, this.ID).then((res) => {
      if (res["success"] === true) {
        this.initComponent();
        this.getAll();
        this.model.reset();
      }
    });
  }

  initComponent() {
    let btnSave = document.getElementById("btnSave");
    let btnUpdate = document.getElementById("btnUpdate");
    btnSave.style.display = "block";
    btnUpdate.style.display = "none";
  }

  editComponent() {
    let btnSave = document.getElementById("btnSave");
    let btnUpdate = document.getElementById("btnUpdate");
    btnSave.style.display = "none";
    btnUpdate.style.display = "block";
  }

  deletes(id: any) {
    let r = window.confirm("¿Esta seguro de eliminar " + id + " ?");
    if (r == true) {
      this._prueba.delete(id).then(() => {
        this.getAll();
      });
    }
  }

}
