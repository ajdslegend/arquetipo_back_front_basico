import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { CommonService } from '../../common/common.service';
import { Router } from '@angular/router';
import { SecurityService } from '../../services/security.service';

import { Constant } from '../../constant/constant';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model: FormGroup;
  conversionEncryptOutput: any;

  constructor(private _common: CommonService,
    private _route: Router,
    private _security: SecurityService,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.model = this.formBuilder.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]]
    });
  }

  submit(form: NgForm) {
    console.log(form);
    form["password"] = CryptoJS.AES.encrypt("123456", form["password"]).toString();
    form["username"]=form["email"];
    delete form["email"];
    let payload = JSON.stringify(form);

    console.log(payload);
    this._security.login(payload).then((res) => {
      console.log(res);
      return;
      if (res['success'] === true) {
        let data = this._common.getResultData(res);
        setTimeout(() => {
          localStorage.setItem('token', data['token']);
          // this._route.navigate(['./' + Constant.ROUTE.REDIRECT_TO]);
        }, 100);
      }
    })

  }

}
