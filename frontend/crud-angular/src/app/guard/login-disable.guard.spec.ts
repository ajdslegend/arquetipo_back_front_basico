import { TestBed, async, inject } from '@angular/core/testing';

import { LoginDisableGuard } from './login-disable.guard';

describe('LoginDisableGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginDisableGuard]
    });
  });

  it('should ...', inject([LoginDisableGuard], (guard: LoginDisableGuard) => {
    expect(guard).toBeTruthy();
  }));
});
