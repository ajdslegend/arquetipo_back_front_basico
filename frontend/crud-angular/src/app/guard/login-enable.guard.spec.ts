import { TestBed, async, inject } from '@angular/core/testing';

import { LoginEnableGuard } from './login-enable.guard';

describe('LoginEnableGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoginEnableGuard]
    });
  });

  it('should ...', inject([LoginEnableGuard], (guard: LoginEnableGuard) => {
    expect(guard).toBeTruthy();
  }));
});
