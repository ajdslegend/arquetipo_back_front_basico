import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { Constant } from '../constant/constant';

@Injectable({
  providedIn: 'root'
})
export class LoginDisableGuard implements CanActivate {

  constructor(private _route: Router) {
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    let token = localStorage.getItem('token');
    if (!token) {
      return true;
    } else {
      this._route.navigate(['./' + Constant.ROUTE.REDIRECT_TO]);
      return false;
    }
  }

}
