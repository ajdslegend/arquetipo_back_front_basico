import { Injectable } from '@angular/core';

import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorToken implements HttpInterceptor {
  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers: any;
    const token: any = localStorage.getItem('token');
    if (token) {
      headers = {
        'Content-Type': 'application/json',
        'Authorization': token
      };
    } else {
      headers = {
        'Content-Type': 'application/json'
      };
    }
    const reqHeaders = req.clone({ setHeaders: headers });
    return next.handle(reqHeaders);
  }
}
