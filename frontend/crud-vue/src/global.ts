// import Vue from 'vue';

// const DOMAIN = 'http://localhost:8000/api/';
// let global;
// global = new Vue({
//   data () {
//     return {
//       LOGIN: DOMAIN + 'login',
//       REGISTER: DOMAIN + 'register',
//       PROFILE: DOMAIN + 'profile',
//       PROFILE_UPDATE: DOMAIN + 'profile/update',
//       ACADEMIC: DOMAIN + 'level-academic/read',
//       COUNTRY: DOMAIN + 'country/read',
//       CHANGE_PASSWORD: DOMAIN + 'reset-password',
//       FORGOT_PASSWORD: DOMAIN + 'forgot-password',
//       RESET_TOKEN_PASSWORD: DOMAIN + 'reset-token-password',
//       PRUEBA_ALL: DOMAIN + 'prueba/all',
//       getBase64ImageEncode (img:any): any {
//         let promise:any = new Promise((resolve, reject) => {
//           let baseString:any;
//           let reader:any = new FileReader();
//           reader.readAsDataURL(img);
//           reader.onload = function () {
//             baseString = reader.result;
//             return resolve(baseString);
//           }
//           reader.onerror = function (error:any) {
//             console.log('Error: ', error);
//             // return reject()
//           }
//         })
//         return promise;
//       }
//     }
//   }
// });

export default class global {

  static DOMAIN = 'http://localhost:8000/api/';

  public static const = {
    LOGIN: global.DOMAIN + 'login',
    REGISTER: global.DOMAIN + 'register',
    PROFILE: global.DOMAIN + 'profile',
    PROFILE_UPDATE: global.DOMAIN + 'profile/update',
    ACADEMIC: global.DOMAIN + 'level-academic/read',
    COUNTRY: global.DOMAIN + 'country/read',
    CHANGE_PASSWORD: global.DOMAIN + 'reset-password',
    FORGOT_PASSWORD: global.DOMAIN + 'forgot-password',
    RESET_TOKEN_PASSWORD: global.DOMAIN + 'reset-token-password',
    PRUEBA_ALL: global.DOMAIN + 'prueba/all',
    PRUEBA_EDIT: global.DOMAIN + 'prueba/edit/',
    PRUEBA_SAVE: global.DOMAIN + 'prueba/save',
    PRUEBA_UPDATE: global.DOMAIN + 'prueba/update/',
    PRUEBA_DELETE: global.DOMAIN + 'prueba/delete/',
  }

  getBase64ImageEncode(img: any): any {
    let promise: any = new Promise((resolve, reject) => {
      let baseString: any;
      let reader: any = new FileReader();
      reader.readAsDataURL(img);
      reader.onload = function () {
        baseString = reader.result;
        return resolve(baseString);
      }
      reader.onerror = function (error: any) {
        console.log('Error: ', error);
        // return reject()
      }
    })
    return promise;
  }
}