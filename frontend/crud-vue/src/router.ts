import Vue from 'vue';
import Router from 'vue-router';
// import Home from './views/Home.vue';
import Login from '@/components/Login.vue';
import Index from '@/components/Index.vue';

import store from './store';

Vue.use(Router);

// const routes = [
//   {
//     path: '/login',
//     name: 'login',
//     component: Login,
//   },
//   {
//     path: '/',
//     name: 'home',
//     component: Home,
//   },
//   {
//     path: '/about',
//     name: 'about',
//     // route level code-splitting
//     // this generates a separate chunk (about.[hash].js) for this route
//     // which is lazy-loaded when the route is visited.
//     component: () => import(/* webpackChunkName: "about" */ './views/About.vue'),
//   },
//   {
//     path: '/index',
//     name: 'index',
//     component: Index,
//     meta: { requiresAuth: true },
//   },
// ];

const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/',
    name: 'index',
    component: Index,
    meta: { requiresAuth: true },
  },
  // {
  //   path: '/index',
  //   name: 'index',
  //   component: Index,
  //   meta: { requiresAuth: true },
  // }
];

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

// const router = new Router({
//   base: process.env.BASE_URL,
//   routes,
// });

router.beforeEach((to, from, next) => {
  // check if the route requires authentication and user is not logged in
  if (to.matched.some(route => route.meta.requiresAuth) && !store.state.isLoggedIn) {
    // redirect to login page
    next({ path: '/login' });
    return;
  }
  // if logged in redirect to dashboard
  // if (to.path === '/login' && store.state.isLoggedIn) {
  //   next({ path: '/index' });
  //   return;
  // }

  // if (to.path === '/register' && store.state.isLoggedIn) {
  //   next({ path: '/index' });
  //   return;
  // }
  if (to.path === '/login' && store.state.isLoggedIn) {
    next({ path: '/' });
    return;
  }

  if (to.path === '/register' && store.state.isLoggedIn) {
    next({ path: '/' });
    return;
  }

  next();
});

export default router;
