import axios from 'axios';

axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let access_token = localStorage.getItem('token');
if (access_token) {
  axios.defaults.headers.common['Authorization'] = 'Bearer ' + access_token;
} else {
  console.log('empty tokens');
}
