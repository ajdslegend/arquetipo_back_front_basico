import React, { Component } from 'react';
import global from './constant';

class Home extends Component {
    constructor(props) {
        super(props);
        global.const.isLogout();
        this.handleInput = this.handleInput.bind(this);
        // this.update = this.update.bind(this);
        // this.save = this.save.bind(this);
        this.state = {
            isEdit: false,
            isSave: true,
            data: [],
            columns: [],
            ID: '',
            name: '',
            age: '',
            form: {
                name: '',
                age: ''
            }
        }


    }

    // state = {
    //     data: [],
    //     columns: []
    // }

    getAll() {
        fetch(global.const.PRUEBA_ALL, {
            method: 'GET',
            headers: global.const.AUTHORIZATION
        }).then((response) => response.json()).then((data) => {
            console.log(data);

            let keys = [];
            let jsonData = data.data;
            for (let i = 0; i < jsonData.length; i++) {
                Object.keys(jsonData[i]).forEach(function (key) {
                    if (keys.indexOf(key) === -1) {
                        if (key !== "id") {
                            keys.push(key);
                        }
                    }
                });
            }
            // this.fields = keys;
            this.setState({
                data: data.data,
                columns: keys
            })
        });
    }

    componentDidMount() {
        global.const.isLogout();
        this.getAll();
    }

    // handleInput(event) {
    //     const target = event.target;
    //     // console.log('#target: ' + target);
    //     // console.log('#target.name: ' + target.name);
    //     // console.log('#target.value: ' + target.value);
    //     let formData = Object.assign({}, this.state.form);
    //     formData[target.name] = target.value;
    //     console.log(formData);
    //     this.setState({ formData });
    // }

    handleInput(event) {
        const target = event.target
        this.setState({
            [target.name]: target.value
        })
    }

    edit(id, event) {
        // console.log("event: " + event + " id: " + id);
        console.log("event: " + event );
        fetch(global.const.PRUEBA_EDIT + id, {
            method: 'GET',
            headers: global.const.AUTHORIZATION
        }).then((response) => response.json()).then((data) => {
            // console.log(data);
            if (data.success) {
                let jsonData = data.data;
                this.setState({
                    isEdit: true,
                    isSave: false,
                    ID: id,
                    name: jsonData.name,
                    age: jsonData.age,
                    form: {
                        name: jsonData.name,
                        age: jsonData.age
                    }
                });
            }
        });
    }

    update(event) {
        event.preventDefault()
        fetch(global.const.PRUEBA_UPDATE + this.state.ID, {
            method: 'PUT',
            headers: global.const.AUTHORIZATION,
            body: JSON.stringify({ name: this.state.name, age: this.state.age })
        }).then((response) => response.json()).then((data) => {
            console.log(data);
            if (data.success) {
                // let jsonData = data.data;
                this.initComponent();
                this.getAll();
            }
        });
    }

    save(event) {
        event.preventDefault()
        fetch(global.const.PRUEBA_SAVE, {
            method: 'POST',
            headers: global.const.AUTHORIZATION,
            body: JSON.stringify({ name: this.state.name, age: this.state.age })
        }).then((response) => response.json()).then((data) => {
            console.log(data);
            if (data.success) {
                // let jsonData = data.data;
                this.initComponent();
                this.getAll();
            }
        });
    }

    initComponent() {
        this.setState({
            isEdit: false,
            isSave: true,
            ID: '',
            name: '',
            age: '',
            form: {
                name: '',
                age: ''
            }
        });
    }

    ifDelete(id, event) {
        // console.log("event: " + event + " id: " + id);
        console.log("event: " + event );
        let r = window.confirm("Esta seguro de eliminar " + id + " ?");
        if (r === true) {
            this.deletes(id);
        }
    }

    deletes(id) {
        console.log("#delete id: " + id);
        fetch(global.const.PRUEBA_DELETE + id, {
            method: 'GET',
            headers: global.const.AUTHORIZATION
        })
            .then(() => {
                this.initComponent();
                this.getAll();
            });

    }

    render() {
        const { columns, data } = this.state;
        return (
            <div>
                home
                <form noValidate>
                    <input type="text" name="name" placeholder="name"
                        onChange={this.handleInput} value={this.state.name} /><br></br>
                    <input type="text" name="age" placeholder="age"
                        onChange={this.handleInput} value={this.state.age} /><br></br>

                    {/* <button style={{ display: this.state.isSave ? 'block' : 'none' }} type="submit">
                        Guardar
                    </button>
                    <button style={{ display: this.state.isEdit ? 'block' : 'none' }} type="submit">
                        Actualizar
                    </button> */}
                    {this.state.isSave ? <button type="submit" onClick={this.save.bind(this)}> aceptar</button> : null}
                    {this.state.isEdit ? <button type="submit" onClick={this.update.bind(this)}> Actualizar</button> : null}
                </form>
                <table>
                    <thead>
                        <tr>
                            {columns.map((k) =>
                                <th key={k}>
                                    {k}
                                </th>
                            )}
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {data.map((k) =>
                            <tr key={k.id}>
                                <td>
                                    {k.name}
                                </td>
                                <td>
                                    {k.age}
                                </td>
                                <td>
                                    <button onClick={this.edit.bind(this, k.id)}>editar</button>
                                    <button onClick={this.ifDelete.bind(this, k.id)}>eliminar</button>
                                </td>
                            </tr>
                        )}
                    </tbody>
                </table>
                <br></br>

            </div>);
    }
}

export default Home;