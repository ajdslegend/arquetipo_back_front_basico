import React, { Component } from 'react';
import global from './constant';
// import { Redirect } from 'react-router-dom';

class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
        this.submitHandler = this.submitHandler.bind(this);
        this.handleInput = this.handleInput.bind(this);
        global.const.isLogin();
    }

    handleInput(event) {
        const target = event.target
        this.setState({
            [target.name]: target.value
        })
    }

    submitHandler(event) {
        event.preventDefault()
        // do some sort of verification here if you need to
        // const { from } = this.props.location.state || { from: { pathname: '/home' } }
        console.log(this.state);
        fetch(global.const.LOGIN, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(this.state)
        }).then(response => response.json())
            .then((response) => {
                // console.log(response.data);
                if (response.success) {
                    localStorage.setItem('token', response.data.token);
                    let token;
                    setTimeout(() => {
                        token = localStorage.getItem('token');
                    }, 100);

                    setTimeout(() => {
                        if (token) {
                            // <Redirect to={from} />
                            window.location = '/';
                        }
                    }, 100);
                }
            });
    }

    render() {
        return (
            <div className="Login">
                <h4>Login</h4>
                <form noValidate>
                    <input type="text" name="email" placeholder="email" value={this.state.email} onChange={this.handleInput} />
                    <br></br>
                    <input type="password" name="password" placeholder="password" value={this.state.password} onChange={this.handleInput} />
                    <br></br>
                    <button type="submit" onClick={this.submitHandler}>aceptar</button>
                </form>
            </div>
        );
    }
}

export default Login;