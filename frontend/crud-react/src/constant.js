// import {
//     Route,
//     Redirect} from 'react-router-dom';

export default class global {
    static DOMAIN = 'http://localhost:8000/api/';

    static const = {
        LOGIN: global.DOMAIN + 'login',
        REGISTER: global.DOMAIN + 'register',
        PROFILE: global.DOMAIN + 'profile',
        CHANGE_PASSWORD: global.DOMAIN + 'reset-password',
        FORGOT_PASSWORD: global.DOMAIN + 'forgot-password',
        RESET_TOKEN_PASSWORD: global.DOMAIN + 'reset-token-password',
        PRUEBA_ALL: global.DOMAIN + 'prueba/all',
        PRUEBA_EDIT: global.DOMAIN + 'prueba/edit/',
        PRUEBA_SAVE: global.DOMAIN + 'prueba/save',
        PRUEBA_UPDATE: global.DOMAIN + 'prueba/update/',
        PRUEBA_DELETE: global.DOMAIN + 'prueba/delete/',
        AUTHORIZATION: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + localStorage.getItem('token')
        },
        logout() {
            localStorage.removeItem('token');
            let token = localStorage.getItem('token');
            if (!token) {
                window.location = '/login';
            }
        },
        isLogin() {
            let token = localStorage.getItem('token');
            if (token) {
                window.location = '/';
            }
        },
        isLogout() {
            let token = localStorage.getItem('token');
            if (!token) {
                window.location = '/login';
            }
        }
    }
}