// import React from 'react';
import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import Login from './login';
import Home from './home';
import About from './about';
import global from './constant';

const fakeAuth = {
  isAuthenticated() {
    let token = (localStorage.getItem('token')) ? true : false;
    return token;
  }
}

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route {...rest} render={(props) => (
    fakeAuth.isAuthenticated()
      ? <Component {...props} />
      : <Redirect to={{
        pathname: '/login',
        state: { from: props.location }
      }} />
  )} />
)

class App extends Component {

  constructor(props) {
    super(props);
    let token = localStorage.getItem('token');
    this.token = token;
  }

  // componentDidMount() {
  //   let token = localStorage.getItem('token');
  //   this.token = token;
  // }

  logout() {
    global.const.logout();
  }

  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            {/* <img src={logo} className="App-logo" width="100" height="100" alt="logo" /> */}
            {/* <li><Link to={'/home'} className="nav-link">Home</Link></li> */}
            {this.token ? <li><Link to={'/home'} className="nav-link">Home</Link></li> : null}
            {this.token ? <li onClick={this.logout}>Salir</li> : null}
            {!this.token ? <li><Link to={'/about'} className="nav-link">About</Link></li> : null}
            {!this.token ? <li><Link to={'/login'} className="nav-link">Login</Link></li> : null}

          </header>
          <Switch>
            <Route exact path='/login' component={Login} />
            <PrivateRoute path='/home' component={Home} />
            <Route path='/about' component={About} />
          </Switch>
        </div>
      </Router>

    );
  }
}

export default App;
